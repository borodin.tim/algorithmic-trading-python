# Algorithmic Trading in Python

### Video tutorial: https://www.youtube.com/watch?v=xfzGZB4HhEE
### Code: https://github.com/nickmccullum/algorithmic-trading-python

To work with python (help: https://uoa-eresearch.github.io/eresearch-cookbook/recipe/2014/11/26/python-virtual-env/):
1. Activate virtual environment run: source E:\PythonWorkspace\Scripts\activate
	Or maybe just "E:\PythonWorkspace\Scripts\activate"
2. Type: python -m notebook
	This will open Jupyter
3. Open file algorithmic-trading-python-master/starter_files/001_equal_weight_S&P_500.ipynb

In case some dependency package is missing (like numpy, pandas, scipy...) install it like so "!pip install scipy" from the Jupyter notebook

## Course Outline

* Section 1: Algorithmic Trading Fundamentals
  * What is Algorithmic Trading?
  * The Differences Between Real-World Algorithmic Trading and This Course
* Section 2: Course Configuration & API Basics
  * How to Install Python
  * Cloning The Repository & Installing Our Dependencies
  * Jupyter Notebook Basics
  * The Basics of API Requests
* Section 3: Building An Equal-Weight S&P 500 Index Fund
  * Theory & Concepts
  * Importing our Constituents
  * Pulling Data For Our Constituents
  * Calculating Weights
  * Generating Our Output File
  * Additional Project Ideas
* Section 4: Building A Quantitative Momentum Investing Strategy
  * Theory & Concepts
  * Pulling Data For Our Constituents
  * Calculating Weights
  * Generating Our Output File
  * Additional Project Ideas
* Section 5: Building A Quantitative Value Investing Strategy
  * Theory & Concepts
  * Importing our Constituents
  * Pulling Data For Our Constituents
  * Calculating Weights
  * Generating Our Output File
  * Additional Project Ideas
